package workshop1_1.interfaceexample;

public class JavaDocent extends Persoon implements JavaExpert{

    private boolean ervaringInBedrijfsLeven;

    public JavaDocent(String name, boolean ervaringInBedrijfsLeven){
        super(name);
        this.ervaringInBedrijfsLeven = ervaringInBedrijfsLeven;
    }


    public String vragen(String vraag) {
        return "Ik kijk even dit tentamen na, dan loop ik zo naar je toe om je vraag te beantwoorden";
    }


    public void programmeerCodeSchrijven() {
        System.out.println("klop, lesgeven, practicum begeleiden, klop, klop");
    }
}
