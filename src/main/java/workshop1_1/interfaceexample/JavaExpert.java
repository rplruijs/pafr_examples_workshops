package workshop1_1.interfaceexample;


public interface JavaExpert {

    public String vragen(String vraag);

    public void programmeerCodeSchrijven();

}
