package workshop1_1.interfaceexample;


public class JavaProgrammeur extends Persoon implements JavaExpert{

    public JavaProgrammeur(String name){
        super(name);
    }

    public String vragen(String vraag) {
        return getName()+ " antwoord: Op deze vraag kom ik terug. " +
                "Ik ben nu druk aan het programmeren voor een klant";
    }

    public void programmeerCodeSchrijven() {
        System.out.println("Klop klop klop klop klop");
    }


    public void test(){
        System.out.println("Test!");
    }


}
