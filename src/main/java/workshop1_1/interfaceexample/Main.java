package workshop1_1.interfaceexample;


public class Main {

    public static void main(String[] args){

        JavaExpert expert1 = new JavaDocent("Remco", true);
        JavaExpert expert2 = new JavaProgrammeur("Alan");


        Student student = new Student("Mees", expert2);

        student.stelVraag("Wat is een interface?");

        student.setBegeleider(expert1);
        expert2.programmeerCodeSchrijven();
        expert2.programmeerCodeSchrijven();
        expert2.programmeerCodeSchrijven();
        expert2.programmeerCodeSchrijven();

        student.stelVraag("Wat is een interface?");

        expert1.programmeerCodeSchrijven();




    }
}
