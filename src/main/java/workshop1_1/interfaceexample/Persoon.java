package workshop1_1.interfaceexample;


public abstract class Persoon {

    private String name;

    protected Persoon(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}
