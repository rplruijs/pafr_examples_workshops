package workshop1_1.iteratie1;


public class Persoon {

    private String name;
    private Shape favoriteShape;

    public Persoon(String name, Shape favoriteShape) {
        this.name = name;
        this.favoriteShape = favoriteShape;
    }

    public void setFavoriteShape(Shape favoriteShape) {
        this.favoriteShape = favoriteShape;
    }

    @Override
    public String toString(){
        return name + " his favorite shape is " + favoriteShape;
    }
}
