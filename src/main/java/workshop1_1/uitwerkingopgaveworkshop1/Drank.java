package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public class Drank extends Product {

    public Drank(int id, String omschrijving, double prijs) {
        super(prijs, id, omschrijving);
    }

    @Override
    public int getLevertijd() {
        return 2;
    }

    @Override
    public String toString() {
        return "Drank{}" + super.toString();
    }
}
