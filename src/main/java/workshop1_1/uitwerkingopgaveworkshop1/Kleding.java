package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public class Kleding extends Product {

    private Maat maat;

    public Kleding(int id, String omschrijving, double prijs, Maat maat) {
        super(prijs, id, omschrijving);
        this.maat = maat;
    }

    @Override
    public int getLevertijd() {
        return 1;
    }

    @Override
    public double getPrijs(){

        if(this.maat == Maat.XL){
            return super.getPrijs() * 1.1;
        }
        return super.getPrijs();
    }

    public Maat getMaat() {
        return maat;
    }

    @Override
    public String toString() {
        return "Kleding{" +
                "maat=" + maat +
                '}' + super.toString();
    }
}

