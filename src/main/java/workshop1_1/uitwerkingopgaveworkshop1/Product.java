package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public abstract class Product {

    private double prijs;
    private String omschrijving;
    private int id;

    public Product(double prijs, int id, String omschrijving) {
        this.prijs = prijs;
        this.id = id;
        this.omschrijving = omschrijving;
    }

    public abstract int getLevertijd();

    public double getPrijs() {
        return prijs;
    }

    public int getId(){
        return id;
    }


    @Override
    public String toString() {
        return "Product{" +
                "prijs=" + getPrijs() +
                ", omschrijving='" + omschrijving + '\'' +
                ", id=" + id +
                '}';
    }
}
