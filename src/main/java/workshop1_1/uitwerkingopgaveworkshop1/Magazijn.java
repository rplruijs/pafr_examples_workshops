package workshop1_1.uitwerkingopgaveworkshop1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public class Magazijn {

    private List<Product> products;

    public Magazijn() {
        this.products = new ArrayList<Product>();
    }


    public void opslaan(Product p){
        products.add(p);
    }

    public Product ophalen(int id){
        for(Product p : products){
            if(p.getId() == id){
                return p;
            }
        }
        return null;
    }

    public List<Product> getProducts() {
        return products;
    }
}
