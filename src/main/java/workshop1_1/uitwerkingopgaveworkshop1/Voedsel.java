package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public class Voedsel extends Product{

    public Voedsel(int id, String omschrijving, double prijs) {
        super(prijs, id, omschrijving);
    }

    @Override
    public int getLevertijd() {
        return 1;
    }


    @Override
    public String toString() {
        return "Voedsel{}" + super.toString();
    }
}
