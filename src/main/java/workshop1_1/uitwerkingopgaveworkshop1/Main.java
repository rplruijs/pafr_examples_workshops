package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public class Main {

    public static void main(String[] args) {
        Magazijn magazijn = new Magazijn();

        Product p1 = new Kleding(1, "Blauwe trui", 30, Maat.XL);
        Product p2 = new Kleding(2, "Zwembroek", 10, Maat.L);
        Product p3 = new Kleding(3, "Blauwe trui", 30, Maat.XL);
        Product p4 = new Kleding(4, "Blauwe trui", 30, Maat.XL);

        Product p5 = new Drank(6, "Amstel bier", 10);

        Product p6 = new Voedsel(7, "Amstel bier", 10);


        magazijn.opslaan(p1);
        magazijn.opslaan(p2);
        magazijn.opslaan(p3);
        magazijn.opslaan(p4);
        magazijn.opslaan(p5);
        magazijn.opslaan(p6);

        Product product1 = magazijn.ophalen(1);
        Product product2 = magazijn.ophalen(2);


        for(Product p : magazijn.getProducts()){
            if(p instanceof Kleding){
                System.out.println(((Kleding)p).getMaat());
            }
        }


        System.out.println(product1.toString());
        System.out.println(product2.toString());

    }
}
