package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public enum Maat {

    S, M, L, XL
}
