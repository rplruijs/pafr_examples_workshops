package workshop1_1.uitwerkingopgaveworkshop1;

/**
 * Created by remcoruijsenaars on 16/11/16.
 */
public class Node extends Product {

    private String name;
    private int waarde;

    public Node(double prijs, int id, String omschrijving, String name, int waarde) {
        super(prijs, id, omschrijving);
        this.name = name;
        this.waarde = waarde;
    }

    public String getName() {
        return name;
    }

    @Override
    public int getLevertijd() {
        return 0;
    }

    @Override
    public String toString() {
        return "Node{" +
                "name='" + name + '\'' +
                ", waarde=" + waarde +
                '}';
    }
}
