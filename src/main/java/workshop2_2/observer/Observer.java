package workshop2_2.observer;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public interface Observer {

    public void update();
}
