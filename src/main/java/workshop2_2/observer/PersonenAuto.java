package workshop2_2.observer;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public class PersonenAuto extends VervoersMiddel {


    public PersonenAuto(String id, TreinSignaal subject) {
        super(id, subject);
    }

    public void update()
    {

        super.update();
        switch(subject.getCode()){
            case GROEN:
                System.out.println("Hard doorrijden.");
                break;
            case ROOD:
                System.out.println("Remmen!!");
                break;
            case ORANJE:
                System.out.println("Het kan nog net. Doorrijden dus.");
                break;


        }
    }

    @Override
    public String toString() {
        return "Personenauto with id " + super.toString();
    }
}
