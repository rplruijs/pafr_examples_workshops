package workshop2_2.observer;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public abstract class VervoersMiddel implements Observer {
    private String id;
    protected TreinSignaal subject;

    public VervoersMiddel(String naam, TreinSignaal subject) {
        this.id = naam;
        this.subject = subject;
    }

    @Override
    public void update() {
        System.out.println(toString() + " has received " + subject.getCode());
    }

    @Override
    public String toString() {
        return id;
    }
}
