package workshop2_2.observer;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public class Main {


    final static String QUIT = "Q";
    final static String ADD = "A";
    final static String REMOVE = "R";
    final static String SIGNAL = "S";

    public static void main(String[] args) {
        TreinSignaal treinSignaal = new TreinSignaal();

        while(true){

            showInputInfo();

            Scanner scanner = new Scanner(System.in);
            String inputUser = scanner.nextLine().toUpperCase();

            switch(inputUser){
                case QUIT   : System.exit(0); break;
                case ADD    : addVervoersMiddel(treinSignaal); break;
                case REMOVE : removeVervoersmiddel(treinSignaal); break;
                case SIGNAL : makeSignal(treinSignaal); break;
            }
         }
    }

    private static void showInputInfo(){

        System.out.println("------------------------------------------------------------");
        System.out.println("Enter one of the following letters");
        System.out.println("Q --> Quit this application");
        System.out.println("A --> Add a new vervoersmiddel (PersonenAuto of Vrachtwagen)");
        System.out.println("R --> Remove a vervoersmiddel randomly");
        System.out.println("S --> Let the Trainsignal make a new signal");
        System.out.println("------------------------------------------------------------");

    }

    private static void addVervoersMiddel(TreinSignaal treinSignaal) {

        String id = UUID.randomUUID().toString();
        Observer observer;
        if(new Random().nextBoolean()){
            observer = new PersonenAuto(id, treinSignaal);
        }else{
            observer = new VrachtWagen(id, treinSignaal);
        }

        treinSignaal.registerObserver(observer);

        System.out.println(observer + " is added as een observer");
    }

    private static void removeVervoersmiddel(TreinSignaal treinSignaal) {
        treinSignaal.removeRandomOne();
    }

    private static void makeSignal(TreinSignaal treinSignaal){
        treinSignaal.veranderSignaal();
    }
}
