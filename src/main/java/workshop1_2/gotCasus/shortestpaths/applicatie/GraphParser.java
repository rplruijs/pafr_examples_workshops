package workshop1_2.gotCasus.shortestpaths.applicatie;

import workshop1_2.gotCasus.shortestpaths.models.Country;
import workshop1_2.gotCasus.shortestpaths.models.World;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.stream.Stream;

/**
 * Created by remcoruijsenaars on 13/11/16.
 */
public class GraphParser {


    public static World parse(String fileName){
        World world = new World();

        try (Stream stream = Files.lines(Paths.get(fileName))) {

            Iterator<String> lines = stream.iterator();

            while(lines.hasNext()){
                String currentLine = lines.next();
                if(!currentLine.isEmpty()){
                    process(currentLine, world);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return world;

    }

    private static void process(String currentLine, World world) {
        if(isPath(currentLine)){
            String[] path = currentLine.split("-");
            String from  = path[0].trim();
            String to    = path[1].trim();

            Double costs = Double.parseDouble(path[2].trim());
            Country fromCity = world.getCity(from);
            Country toCity = world.getCity(to);
            if(fromCity != null && toCity != null){
                fromCity.setNeighbour(toCity, costs);
            }
        }else{ //City
            Country city = new Country(currentLine);
            world.addCity(currentLine, city);
        }

    }

    private static boolean isPath(String line){
        String[] path = line.split("-");
        return path.length == 3;
    }

}
