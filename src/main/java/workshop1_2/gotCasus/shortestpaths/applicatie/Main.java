package workshop1_2.gotCasus.shortestpaths.applicatie;



import workshop1_2.gotCasus.shortestpaths.algorithms.AStarAlgorithm;
import workshop1_2.gotCasus.shortestpaths.algorithms.DijkstrasAlgorithm;
import workshop1_2.gotCasus.shortestpaths.algorithms.ShortestPathAlgorithm;
import workshop1_2.gotCasus.shortestpaths.models.Country;
import workshop1_2.gotCasus.shortestpaths.models.World;
import workshop1_2.gotCasus.shortestpaths.models.INode;
import workshop1_2.gotCasus.shortestpaths.models.IPath;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by remcoruijsenaars on 12/11/16.
 */
public class Main {

    public static void main(String[] args) {

        System.out.println("----------------------------------------");

        final String GRAPH_FILE = "got_graph.txt";
        World world = GraphParser.parse(GRAPH_FILE);

        while(true){
            IPath shortestPath = askForShortestPath(world);
            showResult(shortestPath);

            System.out.println("----------------------------------------");
        }
    }

    private static IPath askForShortestPath(World world) {

        final String STARTQ = "Enter start city:";
        final String ENDQ = "Enter end city:";
        final String ALGOQ = "Which algorithm do you want to use?";

        final List<String> POSALGOS = Arrays.asList("DIJKSTRA", "ASTAR");

        Country from = askForCity(STARTQ, world);
        Country to   = askForCity(ENDQ, world);
        ShortestPathAlgorithm algo  = askForAlgorithm(ALGOQ, POSALGOS);

        return world.getShortestPath(from, to, algo);

    }

    private static ShortestPathAlgorithm askForAlgorithm(String question, List<String> algos) {

        System.out.println("Choose one of: " + algos.toString());
        String algo = "DIJKSTRA";
        boolean isCorrect = false;
        while(!isCorrect) {
            Scanner scanner = new Scanner(System.in);
            System.out.println(question);
            algo = scanner.nextLine();
            if(algos.contains(algo)){
                isCorrect = true;
            }else{
                System.out.println("Please enter a valid algorithm");
            }
        }

        if(algo.toUpperCase().equals("DIJKSTRA")){
            return new DijkstrasAlgorithm();
        }
        if(algo.toUpperCase().equals("ASTAR")){
            return new AStarAlgorithm();
        }

        return null;
    }

    private static Country askForCity(String question, World world){
        boolean isCorrect = false;
        Country city = null;

        while(!isCorrect) {
            Scanner scanner = new Scanner(System.in);
            System.out.println(question);
            String from = scanner.nextLine();
            if (world.citiesExist(from)) {
                isCorrect = true;
                city = world.getCity(from);
            }else{
                System.out.println("Unknown city. Please try again");
            }
        }

        return city;
    }


    private static void showResult(IPath shortestPath) {

        if(shortestPath!=null){
            System.out.println("The shortest path is: ");
            for(INode city: shortestPath.getPath()){
                System.out.print(city.getName() + "-->");
            }
            System.out.println("Distance: " + shortestPath.getTotalCosts());
        }else{
            System.out.println("No path possible.");
        }
    }

}
