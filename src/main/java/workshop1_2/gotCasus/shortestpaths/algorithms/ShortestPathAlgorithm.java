package workshop1_2.gotCasus.shortestpaths.algorithms;


import workshop1_2.gotCasus.shortestpaths.models.INode;
import workshop1_2.gotCasus.shortestpaths.models.IPath;

/**
 * Created by remcoruijsenaars on 12/11/16.
 */
public interface ShortestPathAlgorithm {

    public IPath shortestPath(INode startNode, INode endNode);

}
