package workshop1_2.gotCasus.shortestpaths.algorithms;



import workshop1_2.gotCasus.shortestpaths.models.INode;
import workshop1_2.gotCasus.shortestpaths.models.IPath;
import workshop1_2.gotCasus.shortestpaths.models.Path;

import java.util.Arrays;

/**
 * Created by remcoruijsenaars on 23/11/16.
 *
 * Ik speel hier uiteraard vals. Ik geef altijd een pad terug van de startknoop rechtsreeks naar de eindknoop met
 * kosten 0. Dit is uiteraard geen implementie van het A* algoritme. Mocht je dit weekend tijd over hebben, dan
 * daag ik je uit om het A* algoritme te implementeren.

 */
public class AStarAlgorithm implements ShortestPathAlgorithm{

    @Override

    public IPath shortestPath(INode startNode, INode endNode) {

        Path path = new Path();
        path.setCosts(0);
        path.setPath(Arrays.asList(startNode, endNode));

        return path;
    }
}
