package workshop1_2.gotCasus.shortestpaths.models;/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.Map;

/**
 *
 * @author remcoruijsenaars
 */
public interface INode {
    
    public Map<INode, Double> neighboursWithDistances();
    public String getName();
}
