package workshop1_2.gotCasus.shortestpaths.models;

import workshop1_2.gotCasus.shortestpaths.algorithms.ShortestPathAlgorithm;

import java.util.Map;
import java.util.TreeMap;


/**
 * Created by remcoruijsenaars on 12/11/16.
 */
public class World {

    private Map<String, Country> world = new TreeMap<String, Country>(String.CASE_INSENSITIVE_ORDER);

    public World() { }

    public void addCity(String cityName, Country city){
        world.put(cityName, city);
    }


    public IPath getShortestPath(INode from, INode to, ShortestPathAlgorithm algo){

        if(from != null & to!=null){
            return algo.shortestPath(from, to);
        }

        return null; //Consider Option type
    }

    public boolean citiesExist(String... cities) {

        for(String cityName: cities){
            if(world.get(cityName) ==  null){
                return false;
            }
        }
        return true;
    }

    public Country getCity(String from) {
        return world.get(from);
    }
}
