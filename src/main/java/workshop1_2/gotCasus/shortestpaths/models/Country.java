package workshop1_2.gotCasus.shortestpaths.models;



import workshop1_2.gotCasus.shortestpaths.models.INode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by remcoruijsenaars on 12/11/16.
 */
public class Country implements INode {


    private Map<INode, Double> neighbours;
    private String name;


    public Country(String name){
        neighbours = new HashMap<INode, Double>();
        this.name = name;
    }


    public Country(Map<INode, Double> neighbours, String name) {
        this.neighbours = neighbours;
        this.name = name;
    }

    public void setNeighbour(INode neighbour, Double distance){
        neighbours.put(neighbour, distance);
    }


    @Override
    public Map<INode, Double> neighboursWithDistances() {
        return neighbours;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
