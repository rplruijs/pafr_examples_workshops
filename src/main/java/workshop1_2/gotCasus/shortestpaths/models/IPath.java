package workshop1_2.gotCasus.shortestpaths.models;

import java.util.List;

/**
 * Created by remcoruijsenaars on 12/11/16.
 */
public interface IPath {
    public List<INode> getPath();
    public double getTotalCosts();

}
