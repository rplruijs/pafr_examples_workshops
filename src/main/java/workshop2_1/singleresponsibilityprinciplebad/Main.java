package workshop2_1.singleresponsibilityprinciplebad;

import workshop2_1.singleresponsibilityprinciplebad.UserService;

import java.io.IOException;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class Main {

    public static void main(String[] args) {

        UserService userService = new UserService();
        try {
            userService.register("remcoruijsenaars@gmail.com", "test");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
