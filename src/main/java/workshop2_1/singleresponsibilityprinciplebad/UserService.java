package workshop2_1.singleresponsibilityprinciplebad;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.regex.Pattern;


public class UserService {


    // assumes the current class is called MyLogger
    private final static java.util.logging.Logger LOGGER =
            java.util.logging.Logger.getLogger("MyLogger");


    public UserService(){
        LOGGER.setLevel(Level.ALL);
    }

    public void register(String email, String password) throws IOException {


        final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        if(VALID_EMAIL_ADDRESS_REGEX .matcher(email).find()){
            File file = new File("userdatabase.txt");

            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.append(email + " " + password);

            LOGGER.log(Level.INFO, "user " + "(" + email + " " + password + " saved");
            bw.close();
        }else{
            LOGGER.log(Level.WARNING, "Invalid emailadsress");

        }

    }
}
