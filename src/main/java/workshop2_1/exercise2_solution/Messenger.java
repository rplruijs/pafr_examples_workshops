package workshop2_1.exercise2_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public interface Messenger {

    public void askForCard();
    public void tellInvalidCard();
    public void askForPin();
    public void tellInvalidPin();

    public void askForAccount();
    public void tellNotEnoughMoneyInAccount();
    public void tellAmountDeposited();
    public void tellBalance();

}
