package workshop2_1.exercise2_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public interface WithdrawalMessenger {

    public void askForAccount();
    public void tellNotEnoughMoneyInAccount();
    public void tellAmountDeposited();
    public void tellBalance();
    public void askForFeeConfirmation();
}
