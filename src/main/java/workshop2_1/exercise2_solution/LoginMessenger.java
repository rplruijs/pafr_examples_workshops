package workshop2_1.exercise2_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public interface LoginMessenger {

    public void askForCard();
    public void tellInvalidCard();
    public void askForPin();
    public void tellInvalidPin();

}
