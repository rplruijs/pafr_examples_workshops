package workshop2_1.interfacesegregationprinciplebad;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class XeroxSimplePrintMachine implements IMachine{

    public void print() {
        System.out.println("Printing Job");
    }

    public void staple() {}
    public void scan() {}
    public void photoCopy() {}
}
