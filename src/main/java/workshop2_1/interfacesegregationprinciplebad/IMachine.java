package workshop2_1.interfacesegregationprinciplebad;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public interface IMachine {

    public void print();
    public void staple();
    public void scan();
    public void photoCopy();
}
