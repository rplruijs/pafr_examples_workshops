package workshop2_1.interfacesegregationprinciplebad;

public class XeroxMachine implements IMachine {
    public void print() {
        System.out.println("Printing Job");
    }
    public void staple() {
        System.out.println("Stapling Job");
    }
    public void scan() {
        System.out.println("Scan Job");
    }
    public void photoCopy() {
        System.out.println("Photo Copy");
    }
}
