package workshop2_1.openclosedprinciplebad;

public class GraphicEditor{

    public void drawShape(Shape s){
        if(s.mType==1){
            drawCircle((Circle)(s));
        }else if(s.mType==2){
            drawRectangle((Rectangle)(s));
        }
    }

    private void drawCircle(Circle s) {
        System.out.println("Draw Circle");
    }

    private void drawRectangle(Rectangle r) {
        System.out.println("Draw Rectangle");
    }
}