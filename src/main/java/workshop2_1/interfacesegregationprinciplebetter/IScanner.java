package workshop2_1.interfacesegregationprinciplebetter;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public interface IScanner {
    public void scan();
}
