package workshop2_1.interfacesegregationprinciplebetter;

public class XeroxMachine implements IPrinter, IScanner, IStapler, IPhotoCopier {
    public void print() {
        System.out.println("Printing Job");
    }
    public void staple() {
        System.out.println("Stapling Job");
    }
    public void scan() {
        System.out.println("Scan Job");
    }
    public void photoCopy() {
        System.out.println("Photo Copy");
    }
}
