package workshop2_1.interfacesegregationprinciplebetter;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class XeroxSimplePrintMachine implements IPrinter {
    public void print() {
        System.out.println("Printing Job");
    }
}
