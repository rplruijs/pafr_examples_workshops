package workshop2_1.exercise1_solution;

import java.util.List;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class PaymentSystem {

    public Receipt checkOut(List<Item> items, PaymentMethod pm) {

        Receipt receipt = new Receipt();

        double total = 0;
        for (Item item : items) {
            total += item.getPrice();
            receipt.addItem(item);
        }

        Payment p = pm.acceptPayment(total);
        receipt.setPayment(p);

        return receipt;
    }
}
