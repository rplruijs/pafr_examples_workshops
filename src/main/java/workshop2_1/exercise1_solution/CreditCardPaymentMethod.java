package workshop2_1.exercise1_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class CreditCardPaymentMethod implements PaymentMethod{

    public Payment acceptPayment(double amount) {
        Payment payment = new Payment();
        payment.setAmount(amount);
        payment.setApproved(true);

        // Specifieke Creditcard payment functionaliteit....

        return payment;
    }
}
