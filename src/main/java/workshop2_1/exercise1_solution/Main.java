package workshop2_1.exercise1_solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class Main {

    public static void main(String[] args) {

        Item overnachting = new Item(60, "Overnaching");
        Item minibar = new Item(120, "Inhoud minibar");

        List<Item> items = new ArrayList<Item>();
        items.add(overnachting);
        items.add(minibar);

        PaymentSystem paymentSystem = new PaymentSystem();

        CashPaymentMethod cashMethod = new CashPaymentMethod();
        Receipt payment1 = paymentSystem.checkOut(items, cashMethod);
        System.out.println(payment1);

        CreditCardPaymentMethod creditCardMethod = new CreditCardPaymentMethod();
        Receipt payment2 = paymentSystem.checkOut(items, creditCardMethod);
        System.out.println(payment2);

    }
}
