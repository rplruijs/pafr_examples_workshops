package workshop2_1.exercise1_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 *
 */



public class Payment {

    private boolean approved;
    private double amount;



    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }


    @Override
    public String toString() {
        return "Payment{" +
                "approved=" + approved +
                ", amount=" + amount +
                ", paymentType=" +
                '}';
    }
}
