package workshop2_1.exercise1_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public interface PaymentMethod {
    public Payment acceptPayment(double amount);
}
