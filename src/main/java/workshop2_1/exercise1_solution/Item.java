package workshop2_1.exercise1_solution;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class Item {

    private double price;
    private String description;

    public Item(double price, String description) {
        this.price = price;
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Item{" +
                "price=" + price +
                ", description='" + description + '\'' +
                '}';
    }
}
