package workshop2_1.exercise1_solution;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class Receipt {

    private List<Item> items;
    private Payment payment;

    public Receipt(){
        items = new ArrayList<Item>();
    }

    public void addItem(Item item){
        items.add(item);
    }

    public List<Item> getItems() {
        return items;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }


    @Override
    public String toString() {
        return "Receipt{" +
                "items=" + items +
                ", payment=" + payment +
                '}';
    }
}
