package workshop2_1.liskovsubstitionprinciple;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class Main {

    private static Rectangle getNewRectangle(){
        return new Square(10);
    }

    public static void main(String[] args) {
        Rectangle r = getNewRectangle();
        r.setHeight(10);
        r.setWidth(5);

        System.out.println(r.getArea());
    }
}
