package workshop2_1.liskovsubstitionprinciple;


public class Square extends Rectangle {

    public Square(int width) {
        super(width, width);
    }
    @Override
    public void setHeight(int height) {
        setWidthAndHeight(height);
    }

    @Override
    public void setWidth(int width) {
        setWidthAndHeight(width);
    }

    private void setWidthAndHeight(int value){
        super.setHeight(value);
        super.setWidth(value);
    }
}
