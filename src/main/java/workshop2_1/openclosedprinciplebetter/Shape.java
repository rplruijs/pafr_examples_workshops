package workshop2_1.openclosedprinciplebetter;

public abstract class Shape {
    public abstract void draw();
}
