package workshop2_1.exercise1;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class Main {

    public static void main(String[] args) {

        Item overnachting = new Item(60, "Overnaching");
        Item minibar = new Item(120, "Inhoud minibar");

        List<Item> items = new ArrayList<Item>();
        items.add(overnachting);
        items.add(minibar);

        PaymentSystem paymentSystem = new PaymentSystem();
        Receipt payment = paymentSystem.checkOut(items);

        System.out.println(payment);


    }
}
