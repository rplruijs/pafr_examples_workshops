package workshop2_1.exercise1;

import java.util.List;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class PaymentSystem {

    public Receipt checkOut(List<Item> items) {

        Receipt receipt = new Receipt();

        double total = 0;
        for (Item item : items) {
            total += item.getPrice();
            receipt.addItem(item);
        }

        Payment p = acceptCash(total);
        receipt.setPayment(p);

        return receipt;
    }

    private Payment acceptCash(double total){
        Payment payment = new Payment();
        payment.setAmount(total);
        payment.setApproved(true);


        return payment;
    }

}
