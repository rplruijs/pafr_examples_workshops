package workshop2_1.singleresponsibiltyprinciplebetter;

import java.util.logging.Level;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class MyLogger {

    private final static java.util.logging.Logger LOGGER =
            java.util.logging.Logger.getLogger("MyLogger");


    public MyLogger(){
        LOGGER.setLevel(Level.ALL);
    }

    public void logWarning(String text){
        LOGGER.log(Level.WARNING, text);
    }

    public void logInfo(String text){
        LOGGER.log(Level.INFO, text);
    }

}
