package workshop2_1.singleresponsibiltyprinciplebetter;

import java.io.IOException;

public class UserService {

    public void register(String email, String password) throws IOException {

        EmailValidator emailValidator = new EmailValidator();
        boolean emailCorrect = emailValidator.validateEmail(email);

        if(emailCorrect){
            UserDAO userDAO = new UserDAO();
            userDAO.saveUser(email, password);
        }else{
            MyLogger logger = new MyLogger();
            logger.logWarning("Incorrect email-address");
        }
    }
}
