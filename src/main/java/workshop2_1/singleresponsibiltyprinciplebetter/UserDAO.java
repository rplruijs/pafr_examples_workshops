package workshop2_1.singleresponsibiltyprinciplebetter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class UserDAO {

    private final static String FILENAME = "userdatabase.txt";

    public void saveUser(String email, String password) throws IOException{
        File file = new File("FILENAME");

        if (!file.exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.append(email + " " + password);

        bw.close();

        MyLogger myLogger = new MyLogger();
        myLogger.logInfo("User saved to file");
    }
}
