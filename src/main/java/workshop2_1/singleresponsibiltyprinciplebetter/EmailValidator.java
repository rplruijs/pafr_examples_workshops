package workshop2_1.singleresponsibiltyprinciplebetter;

import java.util.regex.Pattern;

/**
 * Created by remcoruijsenaars on 22/11/16.
 */
public class EmailValidator {

    public boolean validateEmail(String email) {
        final Pattern VALID_EMAIL_ADDRESS_REGEX =
                Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

        return VALID_EMAIL_ADDRESS_REGEX.matcher(email).find();
    }
}
