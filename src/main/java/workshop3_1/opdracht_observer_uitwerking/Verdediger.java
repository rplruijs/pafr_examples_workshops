package workshop3_1.opdracht_observer_uitwerking;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class Verdediger extends VoetbalRobot {

    public Verdediger(FictieveCoach coach, String naam) {
        super(coach, naam);
    }

    public void update() {
        spelModus = coach.getLaatseInstructie();
        System.out.println(toString());
    }

    @Override
    public String toString() {
        return "Verdedigend ingestelde robot " + naam + ", huidige modus: " + spelModus;
    }
}
