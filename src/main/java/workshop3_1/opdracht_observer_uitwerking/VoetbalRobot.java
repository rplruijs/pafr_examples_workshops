package workshop3_1.opdracht_observer_uitwerking;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public abstract class VoetbalRobot implements Observer {


    protected FictieveCoach coach;

    protected String naam;
    protected SpelModus spelModus;


    public VoetbalRobot(FictieveCoach coach, String naam) {
        this.coach = coach;
        this.naam = naam;
    }
}
