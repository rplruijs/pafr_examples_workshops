package workshop3_1.opdracht_observer_uitwerking;

import java.util.Random;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class FictieveCoach extends Subscriber {

    private String name;
    private SpelModus laatseInstructie;

    public FictieveCoach(String name) {
        this.name = name;
    }

    public void startCoaching(){

        while(true){
            try {
                Thread.sleep(2000);

                if(new Random().nextBoolean()){
                    laatseInstructie = SpelModus.AANVALLEN;
                    System.out.println(laatseInstructie);
                }else{
                    laatseInstructie = SpelModus.VERDEDIGEN;
                    System.out.println(laatseInstructie);
                }
                notifyObservers();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public SpelModus getLaatseInstructie() {
        return laatseInstructie;
    }
}
