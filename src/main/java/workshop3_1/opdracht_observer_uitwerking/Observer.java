package workshop3_1.opdracht_observer_uitwerking;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public interface Observer {

    public void update();
}
