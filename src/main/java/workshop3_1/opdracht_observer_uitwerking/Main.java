package workshop3_1.opdracht_observer_uitwerking;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class Main {

    public static void main(String[] args) {
        FictieveCoach coach = new FictieveCoach("Ronald Koeman");

        VoetbalRobot v1 = new Verdediger(coach, "Adri van Tichelen");
        VoetbalRobot v2 = new Verdediger(coach, "Berry van Aerle");

        VoetbalRobot a1 = new Aanvaller(coach, "Arnold Mühren");
        VoetbalRobot a2 = new Aanvaller(coach, "Marco van Basten");

//        coach.registerObserver(v1);
//        coach.registerObserver(v2);
//        coach.registerObserver(a1);
        coach.registerObserver(a2);


        coach.startCoaching();
    }
}
