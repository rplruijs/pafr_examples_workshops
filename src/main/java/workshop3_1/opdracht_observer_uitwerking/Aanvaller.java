package workshop3_1.opdracht_observer_uitwerking;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class Aanvaller extends VoetbalRobot {

    private int verdedigendeAkties = 0;
    private final int MINSUCCVERDIGENDEAKTIES = 3;

    public Aanvaller(FictieveCoach coach, String naam) {
        super(coach, naam);
    }

    @Override
    public void update() {
        switch (coach.getLaatseInstructie()){
            case AANVALLEN:
                spelModus = SpelModus.AANVALLEN;
                verdedigendeAkties = 0;
                break;
            case VERDEDIGEN:
                verdedigendeAkties++;
                if(verdedigendeAkties >= MINSUCCVERDIGENDEAKTIES){
                    spelModus = SpelModus.VERDEDIGEN;
                }
                break;
        }

        System.out.println(toString());

    }

    @Override
    public String toString() {
        return "Aanvallende ingestelde robot " + naam + ": " + spelModus;
    }
}
