package workshop3_1.opdracht_observer_uitwerking;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public abstract class Subscriber {


    private List<Observer> observers;

    public Subscriber() {
        this.observers = new ArrayList<Observer>();
    }

    public void registerObserver(Observer observer){
        if(!observers.contains(observer)){
            observers.add(observer);
        }
    }

    protected void removeRandomOne(){

        if(observers.size() > 0){
            int randomIndex = new Random().nextInt(observers.size());
            Observer randomObserver = observers.get(randomIndex);
            removeObserver(randomObserver);

            System.out.println(randomObserver + " is removed as an Observer");
        }

    }

    protected void removeObserver(Observer observer){
        observers.remove(observer);
    }


    protected void notifyObservers(){
        for (Observer observer: observers) {
            observer.update();
        }
    }
}
