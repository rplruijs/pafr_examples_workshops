package workshop3_1.opdracht_observer;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public abstract class VoetbalRobot {

    protected String naam;
    protected SpelModus spelModus;

    public VoetbalRobot(String name) {
        this.naam = name;
    }
}
