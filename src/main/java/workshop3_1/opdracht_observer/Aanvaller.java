package workshop3_1.opdracht_observer;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class Aanvaller extends VoetbalRobot {

    public Aanvaller(String name) {
        super(name);
        spelModus = SpelModus.AANVALLEN;
    }

    @Override
    public String toString() {
        return "Aanvallende ingestelde robot " + naam + ": " + spelModus;
    }
}
