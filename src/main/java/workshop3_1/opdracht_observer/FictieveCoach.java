package workshop3_1.opdracht_observer;

import java.util.Random;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class FictieveCoach {

    private String name;
    private SpelModus huidigeSpelModus;

    public FictieveCoach(String name) {
        this.name = name;
    }

    public void startCoaching(){

        while(true){
            try {
                Thread.sleep(2000);

                if(new Random().nextBoolean()){
                    huidigeSpelModus = SpelModus.AANVALLEN;
                    System.out.println(huidigeSpelModus);
                }else{
                    huidigeSpelModus = SpelModus.VERDEDIGEN;
                    System.out.println(huidigeSpelModus);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
