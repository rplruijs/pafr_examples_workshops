package workshop3_1.opdracht_observer;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public enum SpelModus {

    AANVALLEN, VERDEDIGEN
}
