package workshop3_1.opdracht_observer;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class Main {

    public static void main(String[] args) {
        FictieveCoach coach = new FictieveCoach("Ronald Koeman");

        VoetbalRobot v1 = new Verdediger("Adri van Tichelen");
        VoetbalRobot v2 = new Verdediger("Berry van Aerle");

        VoetbalRobot v3 = new Aanvaller("Arnold Mühren");
        VoetbalRobot v4 = new Aanvaller("Marco van Basten");

        System.out.println(v1.toString());
        System.out.println(v2.toString());
        System.out.println(v3.toString());
        System.out.println(v4.toString());

        coach.startCoaching();
    }
}
