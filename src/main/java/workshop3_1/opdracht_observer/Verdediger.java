package workshop3_1.opdracht_observer;

/**
 * Created by remcoruijsenaars on 30/11/16.
 */
public class Verdediger extends VoetbalRobot {

    public Verdediger(String naam) {
        super(naam);
        spelModus = SpelModus.AANVALLEN;
    }

    @Override
    public String toString() {
        return "Verdedigend ingestelde robot " + naam + ", huidige modus: " + spelModus;
    }
}
