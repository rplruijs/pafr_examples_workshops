package workshop3_1.observer;

import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public class Main {


    final static String QUIT = "Quit";
    final static String ADD = "Add";
    final static String REMOVE = "Remove";
    final static String SIGNAL = "Signal";

    public static void main(String[] args) {
        TreinSignaal treinSignaal = new TreinSignaal();

        while(true){

            showInputInfo();

            Scanner scanner = new Scanner(System.in);
            String[] inputSplitted = scanner.nextLine().split("\\s+");
            String command = inputSplitted[0].trim();


            switch(command){
                case QUIT   : System.exit(0); break;
                case ADD    : addVervoersMiddel(treinSignaal, inputSplitted[1].trim()); break;
                case REMOVE : removeVervoersmiddel(treinSignaal); break;
                case SIGNAL : makeSignal(treinSignaal); break;
            }
         }
    }

    private static void showInputInfo(){

        System.out.println("------------------------------------------------------------");
        System.out.println("Voer één van de onderstaande letter in");
        System.out.println("Quit --> Stop de applicatie");
        System.out.println("Add P --> Voeg een PersonenAuto toe");
        System.out.println("Add V --> Voeg een Vrachtwagen toe");
        System.out.println("Add F --> Voeg een F1 toe");
        System.out.println("Remove --> Een random vervoersmiddel verwijderen");
        System.out.println("Signal --> Laat het treinsignaal een nieuw signaal genereren");
        System.out.println("------------------------------------------------------------");

    }

    private static void addVervoersMiddel(TreinSignaal treinSignaal, String soort) {

        VervoersMiddelFactory factory = new VervoersMiddelFactory();

        Observer observer = factory.createVervoersMiddel(soort, treinSignaal);

        treinSignaal.registerObserver(observer);
        System.out.println(observer + " is added as een observer");
    }

    private static void removeVervoersmiddel(TreinSignaal treinSignaal) {
        treinSignaal.removeRandomOne();
    }

    private static void makeSignal(TreinSignaal treinSignaal){
        treinSignaal.veranderSignaal();
    }
}
