package workshop3_1.observer;

import java.util.UUID;

/**
 * Created by remcoruijsenaars on 01/12/16.
 */
public class VervoersMiddelFactory {

    public VervoersMiddel createVervoersMiddel(String code, TreinSignaal treinSignaal){

        String id = UUID.randomUUID().toString();

        VervoersMiddel vervoersMiddel = null;
        if(code.equals("P")){
            vervoersMiddel = new PersonenAuto(id, treinSignaal);
        }else if(code.equals("V")){
            vervoersMiddel = new VrachtWagen(id, treinSignaal);
        }else if(code.equals("F")){
            vervoersMiddel = new Formule1(id, treinSignaal);
        }

        return vervoersMiddel;
    }
}
