package workshop3_1.observer;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public class VrachtWagen extends VervoersMiddel {

    public VrachtWagen(String id, TreinSignaal subject) {
        super(id, subject);
    }

    public void update()
    {

        super.update();
        switch(subject.getCode()){
            case GROEN:
                System.out.println("Doorrijden");
                break;
            case ROOD:
                System.out.println("Hard remmen!!");
                break;
            case ORANJE:
                System.out.println("Rustig remmen en stoppen.");
                break;
        }
    }

    @Override
    public String toString() {
        return "Vrachtwagen with id " + super.toString();
    }
}
