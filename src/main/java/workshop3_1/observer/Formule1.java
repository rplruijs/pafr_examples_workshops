package workshop3_1.observer;

/**
 * Created by remcoruijsenaars on 01/12/16.
 */
public class Formule1 extends VervoersMiddel {


    public Formule1(String id, TreinSignaal subject) {
        super(id, subject);
    }

    public void update()
    {

        super.update();
        switch(subject.getCode()){
            case GROEN:
                System.out.println("Heel Hard doorrijden.");
                break;
            case ROOD:
                System.out.println("Red ik wel, doorijden!!");
                break;
            case ORANJE:
                System.out.println("Hard Doorrijden");
                break;


        }
    }

    @Override
    public String toString() {
        return "F1 with id " + super.toString();
    }
}

