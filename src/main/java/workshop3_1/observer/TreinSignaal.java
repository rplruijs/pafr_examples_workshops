package workshop3_1.observer;

/**
 * Created by remcoruijsenaars on 25/11/16.
 */
public class TreinSignaal extends Subscriber{

    private TreinSignaalCode code;

    public TreinSignaal() {
        this.code = TreinSignaalCode.GROEN;
    }

    public TreinSignaalCode getCode() {
        return code;
    }

    public void veranderSignaal(){
        switch(code){
            case GROEN:
                code = TreinSignaalCode.ORANJE;
                System.out.println("Let op.... ORANJE");
                break;
            case ORANJE:
                code = TreinSignaalCode.ROOD;
                System.out.println("Let op.... ROOD!!!");
                break;
            case ROOD:
                System.out.println("Let op.... GROEN!!!");
                code = TreinSignaalCode.GROEN;
                break;
        }

        notifyObservers();
    }

    public void removeRandomOne() {
        super.removeRandomOne();
    }
}
